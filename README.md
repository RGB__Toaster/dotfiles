# RGB__Toaster's Gentoo Dotfiles

## Installation

### Clone The Repository using git
```sh
git clone https://gitlab.com/RGB__Toaster/dotfiles.git
```

### Copy the contents to your home directory
```sh
cp -r dotfiles/* $HOME
```

### Install Zsh

#### Gentoo Linux
```sh
sudo emerge -av zsh
```

#### Debian Based Linux Distros
```sh
sudo apt install zsh
```

#### Arch Linux
```sh
sudo pacman -S zsh
```

### Add the zsh config directory to zprofile
```sh
echo -en "\nexport ZDOTDIR=\"$HOME/.config/zsh\"" | sudo tee -a /etc/zsh/zprofile
```

## Packages

### Needed Packages
- Alacritty
- FastFetch
- Zsh
- Eza
- Hyprland
- Wofi
- Git

### Optional Packages
- Rsmixer
- Bat
- Htop
- Nvtop
- Neovim
- Npm
- Wget
- Tmux

## License

### This Project is currently being Licenced under the <a href="https://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0 Deed</a> License

