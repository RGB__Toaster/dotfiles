#!/usr/bin/env zsh

### ENVIROMENT VARIABLES ###
. $HOME/.config/zsh/exports.zsh

### CARGO ###
. "$HOME/.local/share/cargo/env"

### NEOFETCH ###
echo -n "\n" && fastfetch

### STYLING ###
. $HOME/.config/zsh/style.zsh

### ALIASES ###
. $HOME/.config/zsh/aliases.zsh

### KEYBINDS ###
bindkey "^[[1;5C" forward-word # Ctrl + right arrow
bindkey "^[[1;5D" backward-word # Ctrl + left arrow
bindkey "^[[3~" delete-char # Del key
bindkey "^A" beginning-of-line # Jump to beginning of the line

### IGNORE HIST DUPS ###
setopt hist_ignore_all_dups

### FUNCTIONS ###
. $HOME/.config/zsh/functions.zsh
