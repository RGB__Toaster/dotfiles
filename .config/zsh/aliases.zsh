#!/usr/bin/env zsh

### Git ###
alias dotfiles="git --git-dir=$HOME/dotfiles/ --work-tree=$HOME"
alias clone="git clone"
alias add="git add"

### Replace ls with eza and cat with bat ###
alias ls="eza -al --icons --group-directories-first"
alias cat="bat"

### Shorten usefull commands ###
alias ..="cd .."
alias ~="cd ~"
alias c="clear"

### Add recursive and interactive flag to cp and rm ###
alias cp="cp -ri"
alias rm="rm -ri"

### Neovim and Python3 aliases ###
alias py="python3"
alias nv="nvim"

### Temporary Directory Alias ###
alias tmp="cd $(mktemp --directory)"

### Doas ###
alias sudo="doas"

### Gentoo ###
alias esearch="command emerge --search"
alias eupdate="doas emerge --ask --verbose --update --deep --newuse @world"
alias emerge="doas emerge"
