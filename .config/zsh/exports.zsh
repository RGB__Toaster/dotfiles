#!/usr/bin/env zsh

### XDG ###
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME"=$HOME/.local/state"

### GPG ###
export GPG_TTY=$(tty)
export GNUPGHOME="$XDG_DATA_HOME/gnupg"

### ZSH History ###
export HISTFILE="$XDG_STATE_HOME/zsh/history"
export HISTSIZE=10000
export SAVEHIST=$HISTSIZE
export HISTORY_IGNORE="(history)"

### Java ###
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java"
export JAVA_HOME="/usr/lib/jvm/openjdk-bin-17"
export M2_HOME="/opt/maven"
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"

### Timezone ###
export TZ="Europe/Berlin"

### Runtime Configurations (Rc's) ###
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export WGETRC="$XDG_CONFIG_HOME/wgetrc"

### Homes ###
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export CARGO_HOME="$XDG_DATA_HOME/cargo"

### PATHS ###
export PATH="/usr/lib/ccache/bin:${PATH}"
export PATH="$PATH:$CARGO_HOME/bin"
export PATH="$HOME/.local/bin:${PATH}"
export GOPATH="$XDG_DATA_HOME/go"
export ZSH_PLUGIN_PATH="/usr/share/zsh/plugins"

### C and C++ Compiler ###
export CC="gcc"
export CXX="g++"

### Wayland / Hyprland / Xwayland ###
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
export SDL_IM_MODULE=fcitx
export GLFW_IM_MODULE=ibus

### CCACHE ###
export USE_CCACHE=1
export CCACHE_EXEC="/usr/bin/ccache"

### Terminal and Prompt ###
export TERM="xterm-256color"
export TERMINAL="alacritty"
export PROMPT="starship"

### MISC ###
export ANDROID_HOME="$XDG_DATA_HOME/android"
export GOMODCACHE="$XDG_CACHE_HOME/go/mod"
export W3M_DIR="$XDG_STATE_HOME/w3m"
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export EDITOR="nvim"
export VISUAL="$TERMINAL -e $EDITOR"
export PAGER="bat"
export GRIM_DEFAULT_DIR="$(xdg-user-dir PICTURES)/Screenshots"
export BROWSER="librewolf"
export multithread_glsl_compiler=1
