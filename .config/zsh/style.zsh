#!/usr/bin/env zsh

### PROMPT ###
PROMPT='%B%F{013}   %B%F{002}[ %~ ]%B%F{006} %B%F{004} %B%F{015} '
RPROMPT='%B%F{006}$(parse_git_branch)%F{012}$(parse_git_dirty) %B%F{015}'

### AUTOCOMPLETION ###
autoload -Uz compinit
setopt PROMPT_SUBST
compinit
zstyle ':completion:*' menu select

### SYNTAX HIGHLIGHTING ###
if [ -f "$ZSH_PLUGIN_PATH/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh" ]; then
    source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
elif [ -f "$ZSH_PLUGIN_PATH/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ]; then
    source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

### AUTOSUGGESTIONS ###
. /usr/share/zsh/site-functions/zsh-autosuggestions.zsh
